const problem6 = inventory => {
  
  if (!Array.isArray(inventory)) {
    throw new Error('Inventory is not an Array of Cars');
  }

  const filteredCars = inventory.reduce((target, cars) => {
    if (cars.car_make === 'BMW' || cars.car_make === 'Audi') {
      target.push(cars);
    }
    return target;
  }, []);

  return console.log(JSON.stringify(filteredCars, '', 1));
};

module.exports = problem6;
