const problem2 = inventory => {
  
  if (!Array.isArray(inventory)) {
    throw new Error('Inventory is not an Array of Cars');
  }

  const lastCarId = inventory.length;
  let lastCar = null;
  inventory.filter(carData => {
    if (carData.id == lastCarId) {
      lastCar = `Last car is a ${carData.car_make} ${carData.car_model}`;
    }
  });
  return console.log(lastCar);
};

module.exports = problem2;
