const vows = require('vows');
const assert = require('assert');
const util = require('util');
util.print = console.log;
util.puts = console.log;

const inventory = require('../public/src/inventory');
const problem6 = require('../problem6');

vows
  .describe('array that contains only BMW and Audi cars lot')
  .addBatch({
    'when inventory is not an array': {
      topic: () => {
        try {
          problem6({}, 33);
          return null;
        } catch (error) {
          return error.message;
        }
      },
      'should throw an error': result => {
        assert.strictEqual(result, 'Inventory is not an Array of Cars');
      },
    },
    'BMW && AUDI cars': {
      topic: problem6(inventory),
      'should not throw an error': result => {
        assert.doesNotThrow(() => result);
      },
    },
  })
  .run();
