const vows = require('vows');
const assert = require('assert');
const util = require('util');
util.print = console.log;
util.puts = console.log;

const inventory = require('../public/src/inventory');
const problem1 = require('../problem1');

vows
  .describe('Car Id function')
  .addBatch({
    'when providing an id within inventory range': {
      topic: problem1(inventory, 33),
      'should not throw an error': result => {
        assert.doesNotThrow(() => result);
      },
    },
    'when inventory is not an array': {
      topic: () => {
        try {
          problem1({}, 33);
          return null;
        } catch (error) {
          return error.message;
        }
      },
      'should throw an error': result => {
        assert.strictEqual(result, 'Inventory is not an Array of Cars');
      },
    },
    'when providing an id outside inventory range': {
      topic: () => {
        try {
          problem1(inventory, 60);
          return null;
        } catch (error) {
          return error.message;
        }
      },
      'should throw an error ': result => {
        assert.strictEqual(result, 'Id range exceeds the Inventory length');
      },
    },
    'when id is not a number': {
      topic: () => {
        try {
          problem1(inventory, 'string');
          return null;
        } catch (error) {
          return error.message;
        }
      },
      'should throw an error': result => {
        assert.strictEqual(result, 'Id is not a valid number');
      },
    },
  })
  .run();
