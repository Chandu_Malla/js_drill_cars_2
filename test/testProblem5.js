const vows = require('vows');
const assert = require('assert');
const util = require('util');
util.print = console.log;
util.puts = console.log;

const inventory = require('../public/src/inventory');
const problem5 = require('../problem5');
const problem4 = require('../problem4');

vows
  .describe('cars older than the year 2000')
  .addBatch({
    'when inventory is not an array': {
      topic: () => {
        try {
          problem5({}, 33);
          return null;
        } catch (error) {
          return error.message;
        }
      },
      'should throw an error': result => {
        assert.strictEqual(result, 'Inventory is not an Array of Cars');
      },
    },
    'array of older cars and log its length': {
      topic: problem5(inventory, problem4(inventory)),
    },
    'should not throw an error': result => {
      assert.doesNotThrow(() => result);
    },
  })
  .run();
