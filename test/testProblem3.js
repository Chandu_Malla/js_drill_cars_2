const vows = require('vows');
const assert = require('assert');
const util = require('util');
util.print = console.log;
util.puts = console.log;

const inventory = require('../public/src/inventory');
const problem3 = require('../problem3');

vows
  .describe('Car models listed alphabetically')
  .addBatch({
    'when inventory is not an array': {
      topic: () => {
        try {
          problem3({}, 33);
          return null;
        } catch (error) {
          return error.message;
        }
      },
      'should throw an error': result => {
        assert.strictEqual(result, 'Inventory is not an Array of Cars');
      },
    },
    'sorted car models alphabetically': {
      topic: problem3(inventory),
      'should not throw an error': result => {
        assert.doesNotThrow(() => result);
      },
    },
  })
  .run();
