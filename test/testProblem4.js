const vows = require('vows');
const assert = require('assert');
const util = require('util');
util.print = console.log;
util.puts = console.log;

const inventory = require('../public/src/inventory');
const problem4 = require('../problem4');

vows
  .describe('all the years from every car on the lot')
  .addBatch({
    'when inventory is not an array': {
      topic: () => {
        try {
          problem4({}, 33);
          return null;
        } catch (error) {
          return error.message;
        }
      },
      'should throw an error': result => {
        assert.strictEqual(result, 'Inventory is not an Array of Cars');
      },
    },
    'all car years on the lot': {
      topic: problem4(inventory),
      'should not throw an error': result => {
        console.log(result);
        assert.doesNotThrow(() => result);
      },
    },
  })
  .run();
