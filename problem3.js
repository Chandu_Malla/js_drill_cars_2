const problem3 = inventory => {
  
  if (!Array.isArray(inventory)) {
    throw new Error('Inventory is not an Array of Cars');
  }

  return console.log(inventory.map(data => data.car_model).sort());
};

module.exports = problem3;
