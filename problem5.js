const problem5 = (inventory, listOfCarYears) => {
  
  if (!Array.isArray(inventory)) {
    throw new Error('Inventory is not an Array of Cars');
  } else if (listOfCarYears.length === 0 || !Array.isArray(listOfCarYears)) {
    throw Error('List of car years is not found');
  }

  const cars_before_2000 = listOfCarYears.reduce((count, year) => {
    return count + (year < 2000 ? 1 : 0);
  }, 0);

  const listOfOlderCars = inventory.reduce((olderCars, data) => {
    if (data.car_year > 2000) {
      olderCars.push(data.car_make);
    }
    return olderCars;
  }, []);

  console.log(
    `Cars made before the year 2000: ${cars_before_2000}, array of older cars: ${listOfOlderCars}, total older cars: ${listOfOlderCars.length}`,
  );
};

module.exports = problem5;
