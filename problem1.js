const problem1 = (inventory, id) => {
  
  if (!Array.isArray(inventory)) {
    throw new Error('Inventory is not an Array of Cars');
  } else if (isNaN(id)) {
    throw new Error('Id is not a valid number');
  } else if (id > 50) {
    throw new Error('Id range exceeds the Inventory length');
  }

  let data = 0;
  inventory.filter(cars => {
    if (cars.id == id) {
      data = `Car ${id} is a ${cars.car_year} ${cars.car_make} ${cars.car_model}`;
    }
  });
  return console.log(data);
};

module.exports = problem1;
