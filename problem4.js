const problem4 = inventory => {
  
  if (!Array.isArray(inventory)) {
    throw new Error('Inventory is not an Array of Cars');
  }

  return inventory.map(cars => {
    return cars.car_year;
  });
};

module.exports = problem4;
